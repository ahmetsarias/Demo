package com.evreka.ahmetsari.demo.components;

import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.evreka.ahmetsari.demo.OperationsActivity;
import com.evreka.ahmetsari.demo.R;
import com.evreka.ahmetsari.demo.components.events.OnRelocateClickedListener;
import com.evreka.ahmetsari.demo.components.events.OnRelocateSaveClickedListener;
import com.evreka.ahmetsari.demo.firebase.model.Container;

public class ContainerRelocatePopup {
    private PopupWindow popup;
    private OperationsActivity activity;
    private View popupContent;
    private OnRelocateSaveClickedListener onRelocateSaveClickedListener;

    public ContainerRelocatePopup(OperationsActivity activity, final OnRelocateSaveClickedListener onRelocateSaveClickedListener) {
        this.activity = activity;
        this.onRelocateSaveClickedListener = onRelocateSaveClickedListener;
        popup = new PopupWindow();
        popupContent = activity.getLayoutInflater().inflate(R.layout.popup_relocate, null);
        popup = new PopupWindow();

        //popup should wrap content view
        popup.setWindowLayoutMode(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        //set content and background
        popup.setContentView(popupContent);

        popupContent.findViewById(R.id.btnRelocateSave).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onRelocateSaveClickedListener.onRelocateSaveClicked();
            }
        });

        popup.setFocusable(false);
    }

    public void show(){
        if (!popup.isShowing())
            popup.showAtLocation(activity.findViewById(R.id.map).getRootView(), Gravity.BOTTOM,0,0);
    }

    public void dismiss(){
        popup.dismiss();
    }

    public boolean isShowing(){
        return popup.isShowing();
    }
}
