package com.evreka.ahmetsari.demo.firebase.events;

import com.evreka.ahmetsari.demo.firebase.model.Container;
import com.google.firebase.database.DatabaseError;

public interface OnContainersChangedListener {
    void containerAdded(Container container);
    void containerChanged(Container container);
    void containerRemoved(String id);
    void onContainerListenerError(DatabaseError e);
}
