package com.evreka.ahmetsari.demo.firebase.events;

public interface OnRelocateStatusListener {
    void onRelocateSuccess(String containerId);
    void onRelocateFail(String containerId, Exception e);
}
