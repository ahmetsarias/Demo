package com.evreka.ahmetsari.demo.task.checkinternetconn;

public interface CheckInternetConnectionResponse {
    void processFinish(Boolean output);
}