package com.evreka.ahmetsari.demo.firebase.events;

import com.google.firebase.database.DatabaseError;

import java.util.HashMap;

public interface OnAreaNameReceivedListener {
    void areaNameReceived(String areaName, DatabaseError e);
}
