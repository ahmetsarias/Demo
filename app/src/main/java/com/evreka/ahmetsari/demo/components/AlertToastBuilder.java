package com.evreka.ahmetsari.demo.components;

import android.app.Activity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.evreka.ahmetsari.demo.R;

public class AlertToastBuilder {
     public static Toast make(Activity activity, int textRes, boolean error, int duration){
         LayoutInflater inflater = activity.getLayoutInflater();
         View layout = inflater.inflate(R.layout.alert_toast, (ViewGroup) activity.findViewById(R.id.custom_toast_container));
         TextView tv = (TextView) layout.findViewById(R.id.txtvw);
         tv.setText(textRes);
         ImageView iv = (ImageView) layout.findViewById(R.id.ivErrorIcon);
         if (error){
             iv.setVisibility(View.VISIBLE);
         }else {
             iv.setVisibility(View.INVISIBLE);
         }
         Toast toast = new Toast(activity.getApplicationContext());
         toast.setGravity(Gravity.BOTTOM | Gravity.FILL_HORIZONTAL, 0, 300);
         toast.setDuration(duration);
         toast.setView(layout);
         return toast;
     }
}
