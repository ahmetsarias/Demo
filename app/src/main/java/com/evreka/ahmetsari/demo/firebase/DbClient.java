package com.evreka.ahmetsari.demo.firebase;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.evreka.ahmetsari.demo.firebase.events.OnAreaBoundsReceivedListener;
import com.evreka.ahmetsari.demo.firebase.events.OnAreaNameReceivedListener;
import com.evreka.ahmetsari.demo.firebase.events.OnContainersChangedListener;
import com.evreka.ahmetsari.demo.firebase.events.OnRelocateStatusListener;
import com.evreka.ahmetsari.demo.firebase.model.Container;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DbClient {
    private FirebaseDatabase database;

    public DbClient() {
        this.database = FirebaseDatabase.getInstance();
    }

    public void getAreaNameOfUser(String userId, final OnAreaNameReceivedListener listener){
        final DatabaseReference areaOfUserRef = database.getReference("users/"+userId+"/area");
        areaOfUserRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                listener.areaNameReceived((String) dataSnapshot.getValue(), null);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.areaNameReceived(null, databaseError);
            }
        });
    }

    public void getAreaBoundsOfUser(String areaName, final OnAreaBoundsReceivedListener listener){
        DatabaseReference boundsRef = database.getReference("areas/" + areaName);
        boundsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<LatLng> latLngs = new ArrayList<>();
                DataSnapshot boundsSnapshot = dataSnapshot.child("bounds");
                for (DataSnapshot latLongSnaphot : boundsSnapshot.getChildren()){
                    HashMap<String, Double> latLongMap = (HashMap<String, Double>) latLongSnaphot.getValue();
                    LatLng latLng = new LatLng(latLongMap.get("lat"), latLongMap.get("long"));
                    latLngs.add(latLng);
                }
                LatLng areaPos = new LatLng((Double) dataSnapshot.child("pos").child("lat").getValue(),(Double) dataSnapshot.child("pos").child("long").getValue());
                listener.boundsReceived(areaPos, latLngs,null);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.boundsReceived(null, null, databaseError);
            }
        });
    }

    public void listenContainersOfArea(String areaName, final OnContainersChangedListener listener){
        DatabaseReference containersRef = database.getReference("containers/" + areaName);
        containersRef.addChildEventListener(new ChildEventListener() {
            private Container containerFromSnapshot(DataSnapshot snapshot){
                HashMap<String, Object> container = (HashMap<String, Object>) snapshot.getValue();
                HashMap<String, Object> position = (HashMap<String, Object>) container.get("pos");
                return new Container(snapshot.getKey(),
                        (Long)container.get("fullness"),
                        (String) container.get("nextCollection"),
                        new LatLng((Double) position.get("lat"),(Double) position.get("long")));
            }

            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                 listener.containerAdded(containerFromSnapshot(dataSnapshot));
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                listener.containerChanged(containerFromSnapshot(dataSnapshot));
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                listener.containerRemoved(dataSnapshot.getKey());
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                // No such case in our scenario
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.onContainerListenerError(databaseError);
            }
        });
    }

    public void changeContainerLocation(String areaName, final String containerId, LatLng latLng, final OnRelocateStatusListener listener){
        DatabaseReference positionRef = database.getReference("containers/"+areaName+ "/" + containerId +"/pos");
        HashMap<String, Double> pos = new HashMap<>();
        pos.put("lat", latLng.latitude);
        pos.put("long", latLng.longitude);
        positionRef.setValue(pos)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        // Write was successful
                        listener.onRelocateSuccess(containerId);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Write failed
                        listener.onRelocateFail(containerId, e);
                    }
                });
    }
}
