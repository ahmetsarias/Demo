package com.evreka.ahmetsari.demo;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.evreka.ahmetsari.demo.components.AlertToastBuilder;
import com.evreka.ahmetsari.demo.task.checkinternetconn.CheckInternetConnectionResponse;
import com.evreka.ahmetsari.demo.task.checkinternetconn.CheckInternetConnectionTask;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, CheckInternetConnectionResponse, TextWatcher {

    TextInputLayout tilUsername;
    TextInputLayout tilPassword;
    EditText edtUsername;
    EditText edtPassword;
    Button btnLogin;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //TextFields Initialization
        tilUsername = findViewById(R.id.tfUsername);
        tilPassword = findViewById(R.id.tfPassword);
        edtUsername = tilUsername.getEditText();
        edtPassword = tilPassword.getEditText();
        edtUsername.addTextChangedListener(this);
        edtPassword.addTextChangedListener(this);

        //Login Button Initialization
        btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);

        new CheckInternetConnectionTask(this).execute();

        mAuth = FirebaseAuth.getInstance();
    }

    private void setErrorTextField(TextInputLayout textField, boolean error){
        textField.setError(error?" ":"");
    }

    private void clearErrors(){
        setErrorTextField(tilUsername,false);
        setErrorTextField(tilPassword,false);
    }

    private void setEnableOfAllViews(boolean isEnabled){
        tilUsername.setEnabled(isEnabled);
        tilPassword.setEnabled(isEnabled);
        btnLogin.setEnabled(isEnabled);
    }

    @Override
    public void onClick(View v) {
        clearErrors();
        setEnableOfAllViews(false);
        String email = edtUsername.getText().toString().concat(getResources().getString(R.string.emailExtension));
        String password = edtPassword.getText().toString();
        mAuth.signInWithEmailAndPassword(email, password )
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        setEnableOfAllViews(true);
                        if (task.isSuccessful()) {
                            // Sign in success
                            Intent show = new Intent(LoginActivity.this, OperationsActivity.class);
                            startActivity(show);
                        } else {
                            // If sign in fails, display a message to the user.
                            Exception e = task.getException();
                            int exceptionTextRes = R.string.alertUnexpectedError;
                            if(e instanceof FirebaseNetworkException){
                                exceptionTextRes = R.string.alertNoInternet;
                            }
                            else if(e instanceof FirebaseAuthInvalidUserException){
                                exceptionTextRes = R.string.alertUserNotFound;
                                setErrorTextField(tilUsername,true);
                            }else if (e instanceof FirebaseAuthInvalidCredentialsException){
                                exceptionTextRes = R.string.alertCredentialError;
                                setErrorTextField(tilPassword,true);
                            }
                            AlertToastBuilder.make(LoginActivity.this, exceptionTextRes, true, Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    @Override
    public void processFinish(Boolean hasInternet) {
        if (!hasInternet){
            AlertToastBuilder.make(this, R.string.alertNoInternet, true, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (TextUtils.isEmpty(edtUsername.getText().toString()) || TextUtils.isEmpty(edtPassword.getText().toString())){
            btnLogin.setEnabled(false);
        }else {
            btnLogin.setEnabled(true);
        }
    }

    @Override
    public void afterTextChanged(Editable s) { }
}
