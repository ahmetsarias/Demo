package com.evreka.ahmetsari.demo.firebase.events;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DatabaseError;

import java.util.List;

public interface OnAreaBoundsReceivedListener {
    void boundsReceived(LatLng areaPos, List<LatLng> bounds, DatabaseError e);
}
