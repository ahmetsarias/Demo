package com.evreka.ahmetsari.demo;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.evreka.ahmetsari.demo.components.AlertToastBuilder;
import com.evreka.ahmetsari.demo.components.ContainerInfoPopup;
import com.evreka.ahmetsari.demo.components.ContainerRelocatePopup;
import com.evreka.ahmetsari.demo.components.events.OnRelocateClickedListener;
import com.evreka.ahmetsari.demo.components.events.OnRelocateSaveClickedListener;
import com.evreka.ahmetsari.demo.firebase.DbClient;
import com.evreka.ahmetsari.demo.firebase.events.OnAreaBoundsReceivedListener;
import com.evreka.ahmetsari.demo.firebase.events.OnAreaNameReceivedListener;
import com.evreka.ahmetsari.demo.firebase.events.OnContainersChangedListener;
import com.evreka.ahmetsari.demo.firebase.events.OnRelocateStatusListener;
import com.evreka.ahmetsari.demo.firebase.model.Container;
import com.evreka.ahmetsari.demo.map.render.CustomIconRenderer;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseError;
import com.google.maps.android.clustering.ClusterManager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OperationsActivity extends AppCompatActivity implements OnMapReadyCallback,
        ClusterManager.OnClusterItemClickListener<Container>,
        OnRelocateClickedListener,
        OnRelocateSaveClickedListener,
        GoogleMap.OnMapClickListener, OnRelocateStatusListener {

    GoogleMap googleMap;
    DbClient dbClient;
    private ClusterManager<Container> mClusterManager;
    Map<String, Container> containers;

    private Container selectedContainer;
    private Container tempContainerRelocate;

    String areaName;
    private ContainerInfoPopup containerInfoPopup;
    private ContainerRelocatePopup containerRelocatePopup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_operations);
        // Get the SupportMapFragment and request notification
        // when the map is ready to be used.
        dbClient = new DbClient();
        containers = new HashMap<>();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        containerInfoPopup = new ContainerInfoPopup(this,this);
        containerRelocatePopup = new ContainerRelocatePopup(this,this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        setUpClusterer();
        initializeAreaAndContainers();
        googleMap.setOnMapClickListener(this);
    }

    private void initializeAreaAndContainers(){
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if(currentUser!=null){
            dbClient.getAreaNameOfUser(currentUser.getUid(), new OnAreaNameReceivedListener() {
                @Override
                public void areaNameReceived(String areaName, DatabaseError e) {
                    OperationsActivity.this.areaName = areaName;
                    drawAreaOfUser();
                    handleContainers();
                }
            });
        }
    }

    private void handleContainers(){
        if(areaName != null){
            dbClient.listenContainersOfArea(areaName, new OnContainersChangedListener() {
                @Override
                public void containerAdded(Container container) {
                    OperationsActivity.this.containers.put(container.getId(), container);
                    OperationsActivity.this.mClusterManager.addItem(container);
                    mClusterManager.cluster();
                }

                @Override
                public void containerChanged(Container container) {
                    //Replace with current
                    Container currentContainer = OperationsActivity.this.containers.get(container.getId());
                    OperationsActivity.this.mClusterManager.removeItem(currentContainer);

                    OperationsActivity.this.containers.put(container.getId(), container);
                    OperationsActivity.this.mClusterManager.addItem(container);
                    mClusterManager.cluster();
                }

                @Override
                public void containerRemoved(String id) {
                    Container currentContainer = OperationsActivity.this.containers.get(id);
                    OperationsActivity.this.mClusterManager.removeItem(currentContainer);
                    mClusterManager.cluster();
                }

                @Override
                public void onContainerListenerError(DatabaseError e) {
                    AlertToastBuilder.make(OperationsActivity.this, R.string.alertUnexpectedError,true, Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    private void setUpClusterer() {
        // Initialize the manager with the context and the map.
        mClusterManager = new ClusterManager<>(this, googleMap);
        mClusterManager.setAnimation(false);
        mClusterManager.setRenderer(new CustomIconRenderer(this,googleMap,mClusterManager));
        // Point the map's listeners at the listeners implemented by the cluster
        // manager.
        googleMap.setOnCameraIdleListener(mClusterManager);
        googleMap.setOnMarkerClickListener(mClusterManager);
        mClusterManager.setOnClusterItemClickListener(this);
    }

    private void drawAreaOfUser(){
        if(areaName != null){
            dbClient.getAreaBoundsOfUser(areaName, new OnAreaBoundsReceivedListener() {
                @Override
                public void boundsReceived(LatLng areaPos, List<LatLng> bounds, DatabaseError e) {
                    if (e == null){
                        Polyline area = googleMap.addPolyline(new PolylineOptions()
                                .addAll(bounds));
                        area.setColor(getResources().getColor(R.color.colorPrimary, getTheme()));
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(areaPos, 14));
                    }
                }
            });
        }
    }

    @Override
    public boolean onClusterItemClick(Container container) {
        if (container.getId() != null){
            if (tempContainerRelocate != null){
                mClusterManager.removeItem(tempContainerRelocate);
                mClusterManager.cluster();
            }
            containerRelocatePopup.dismiss();

            //Show Container Info
            if (selectedContainer != null){
                selectedContainer.setSelectedState(Container.SelectedState.NONE, this);
            }
            this.selectedContainer = container;
            container.setSelectedState(Container.SelectedState.SELECTED, this);
            containerInfoPopup.show(container);
        }
        return false;
    }

    @Override
    public void onRelocateClicked(String containerId) {
        containerInfoPopup.dismiss();
        containerRelocatePopup.show();
    }

    @Override
    public void onMapClick(LatLng latLng) {
        if (containerRelocatePopup.isShowing()){ //In Relocation Duration
            selectedContainer.setSelectedState(Container.SelectedState.CHANGING_POSITION, this);
            if (tempContainerRelocate != null){
                mClusterManager.removeItem(tempContainerRelocate);
            }
            tempContainerRelocate = new Container(latLng);
            tempContainerRelocate.setSelectedState(Container.SelectedState.SELECTED,this);
            mClusterManager.addItem(tempContainerRelocate);
            mClusterManager.cluster();
        }
    }

    @Override
    public void onRelocateSaveClicked() {
        //Save Changes
        containerRelocatePopup.dismiss();
        mClusterManager.removeItem(tempContainerRelocate);
        selectedContainer.setSelectedState(Container.SelectedState.NONE,this);
        dbClient.changeContainerLocation(areaName, selectedContainer.getId(), tempContainerRelocate.getPosition(), this);
        selectedContainer = null;
        tempContainerRelocate = null;

    }

    @Override
    public void onRelocateSuccess(String containerId) {
        AlertToastBuilder.make(this, R.string.relocateSuccessDesc, false, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRelocateFail(String containerId, Exception e) {
        //TODO Log this
        mClusterManager.cluster();
        AlertToastBuilder.make(this, R.string.alertUnexpectedError, true, Toast.LENGTH_LONG).show();
    }
}
