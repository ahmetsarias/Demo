package com.evreka.ahmetsari.demo.firebase.model;

import android.content.Context;

import com.evreka.ahmetsari.demo.R;
import com.evreka.ahmetsari.demo.map.render.CustomIconRenderer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.clustering.ClusterItem;

public class Container implements ClusterItem {
    private String id;
    private Long fullness;
    private String nextCollection;
    private LatLng position;
    private String title;
    private String snippet;
    private int icon;
    private Marker marker;

    public Container() {
        //Default Icon
        this.icon = R.drawable.ic_household_bin;
    }

    public Container(LatLng latLng) {
        this();
        position = latLng;
    }

    public Container(String id, Long fullness, String nextCollection, LatLng position) {
        this();
        this.id = id;
        this.fullness = fullness;
        this.nextCollection = nextCollection;
        this.position = position;
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getFullness() {
        return fullness;
    }

    public void setFullness(Long fullness) {
        this.fullness = fullness;
    }

    public String getNextCollection() {
        return nextCollection;
    }

    public void setNextCollection(String nextCollection) {
        this.nextCollection = nextCollection;
    }

    public Marker getMarker() {
        return marker;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }

    @Override
    public LatLng getPosition() {
        return position;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getSnippet() {
        return snippet;
    }

    public int getIcon() {
        return icon;
    }

    public void setSelectedState(SelectedState state, Context context){
        switch (state){
            case NONE:
                this.icon = R.drawable.ic_household_bin;
                break;
            case SELECTED:
                this.icon = R.drawable.ic_battery_bin;
                break;
            case CHANGING_POSITION:
                this.icon = R.drawable.ic_battery_bin_relocating;
                break;
        }
        updateMarker(context);
    }
    private void updateMarker(Context context) {
        if (getMarker() != null) {
            getMarker().setIcon(CustomIconRenderer.getMarkerIconFromDrawable(context.getResources().getDrawable(getIcon())));
        }
    }

    public static enum SelectedState {
        NONE,
        SELECTED,
        CHANGING_POSITION
    }
}
