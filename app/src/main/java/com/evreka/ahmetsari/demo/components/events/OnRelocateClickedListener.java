package com.evreka.ahmetsari.demo.components.events;

public interface OnRelocateClickedListener {
    void onRelocateClicked(String containerId);
}
