package com.evreka.ahmetsari.demo.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.evreka.ahmetsari.demo.R;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Objects;

import androidx.annotation.Nullable;
import androidx.appcompat.content.res.AppCompatResources;

public class ClearableEditText extends TextInputEditText {

    private Drawable drawableClear;
    private Drawable drawableError;

    int actionX, actionY;


    public ClearableEditText(Context context) {
        super(context);
    }

    public ClearableEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs,R.styleable.ClearableEditText, 0, 0);
        try {
            int drawableResId = a.getResourceId(R.styleable.ClearableEditText_drawableClear, -1);
            if (drawableResId != -1)
                drawableClear = AppCompatResources.getDrawable(context, drawableResId);

            drawableResId = a.getResourceId(R.styleable.ClearableEditText_drawableError, -1);
            if (drawableResId != -1)
                drawableError = AppCompatResources.getDrawable(context, drawableResId);
        }
        finally {
            a.recycle();
        }
    }

    public ClearableEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        if (text.toString().length() > 0) {
            setCompoundDrawablesWithIntrinsicBounds(null, null, drawableClear, null);
        } else {
            setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        }
        super.onTextChanged(text, start, lengthBefore, lengthAfter);
    }

    @Override
    public void setError(@Nullable CharSequence errorText) {
        if (!TextUtils.isEmpty(errorText)){
            if (drawableError != null){
                setCompoundDrawablesWithIntrinsicBounds(null, null, drawableError, null);
            }
        }else{
            if (!TextUtils.isEmpty(Objects.requireNonNull(this.getText()).toString())){
                setCompoundDrawablesWithIntrinsicBounds(null, null, drawableClear, null);
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Rect bounds;
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            actionX = (int) event.getX();
            actionY = (int) event.getY();
            if (drawableClear != null) {

                bounds = drawableClear.getBounds();
                int x, y;
                int extraTapArea = 13;

                x = actionX + extraTapArea;
                y = actionY - extraTapArea;

                x = getWidth() - x;

                if (x <= 0) {
                    x += extraTapArea;
                }

                if (y <= 0)
                    y = actionY;

                if (bounds.contains(x, y)) {
                    this.setText("");
                    event.setAction(MotionEvent.ACTION_CANCEL);
                    return false;
                }
            }

        }
        return super.onTouchEvent(event);
    }

    @Override
    protected void finalize() throws Throwable {
        drawableClear = null;
        super.finalize();
    }

}
