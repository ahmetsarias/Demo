package com.evreka.ahmetsari.demo.components;

import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.evreka.ahmetsari.demo.OperationsActivity;
import com.evreka.ahmetsari.demo.R;
import com.evreka.ahmetsari.demo.components.events.OnRelocateClickedListener;
import com.evreka.ahmetsari.demo.firebase.model.Container;

public class ContainerInfoPopup {
    private PopupWindow popup;
    private OperationsActivity activity;
    private View popupContent;
    private TextView txtContainerId;
    private TextView txtNextCollection;
    private TextView txtFullnessRate;
    private OnRelocateClickedListener onRelocateClickedListener;

    public ContainerInfoPopup(OperationsActivity activity, final OnRelocateClickedListener onRelocateClickedListener) {
        this.activity = activity;
        this.onRelocateClickedListener=onRelocateClickedListener;
        popup = new PopupWindow();
        popupContent = activity.getLayoutInflater().inflate(R.layout.popup_container_info, null);
        popup = new PopupWindow();

        //popup should wrap content view
        popup.setWindowLayoutMode(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        //set content and background
        popup.setContentView(popupContent);
        txtContainerId = popupContent.findViewById(R.id.txtContainerId);
        txtNextCollection = popupContent.findViewById(R.id.txtNextCollection);
        txtFullnessRate = popupContent.findViewById(R.id.txtFullness);

        popupContent.findViewById(R.id.btnRelocate).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onRelocateClickedListener.onRelocateClicked(txtContainerId.getText().toString());
            }
        });

        popup.setFocusable(true);
        popup.setOutsideTouchable(true);
    }

    public void show(Container container){
        txtContainerId.setText(container.getId());
        txtNextCollection.setText(container.getNextCollection());
        txtFullnessRate.setText("%"+container.getFullness().toString());
        if (!popup.isShowing())
            popup.showAtLocation(activity.findViewById(R.id.map).getRootView(), Gravity.BOTTOM,0,0);
    }

    public void dismiss(){
        popup.dismiss();
    }

    public boolean isShowing(){
        return popup.isShowing();
    }
}
