package com.evreka.ahmetsari.demo.components.events;

public interface OnRelocateSaveClickedListener {
    void onRelocateSaveClicked();
}
