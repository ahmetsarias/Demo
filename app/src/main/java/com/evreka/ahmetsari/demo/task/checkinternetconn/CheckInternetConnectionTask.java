package com.evreka.ahmetsari.demo.task.checkinternetconn;

import android.os.AsyncTask;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

public class CheckInternetConnectionTask extends AsyncTask<Void, Void, Boolean> {

    private Exception exception;
    private CheckInternetConnectionResponse delegate;

    public CheckInternetConnectionTask(CheckInternetConnectionResponse delegate) {
        this.delegate = delegate;
    }

    protected Boolean doInBackground(Void... voids) {
        try {
            int timeoutMs = 1500;
            Socket sock = new Socket();
            SocketAddress sockaddr = new InetSocketAddress("8.8.8.8", 53);

            sock.connect(sockaddr, timeoutMs);
            sock.close();

            return true;
        } catch (IOException e) { return false; }
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        delegate.processFinish(aBoolean);
    }
}