# Evreka Demo


## Test Credentials

***Username***: test

***Password***: 123456

## Notes
You can find apk file from [here](https://gitlab.com/ahmetsarias/Demo/-/tree/master/debug_apk)

I used firebase realtime database for db purposes. You can find db export json [here](https://gitlab.com/ahmetsarias/Demo/-/tree/master/FirebaseRealTimeDbExport)

Container data changes are updated in real time among users who share same area. You can test it with two phones.

For performance purposes, I used Marker Clustering on map. I couldn't test with about 200 markers. But if there is a problem about 200 markers or more, we can easily change clustering algorithm like rendering only markers between camera bounds etc.

